import { YouTubeVideo } from '@/model/youtube-video';

export async function getYoutubeVideos(): Promise<YouTubeVideo[]> {
  const res = await fetch('https://json-server-vercel-for-tutorials.vercel.app/youtube-videos', {
    // next: { revalidate: 10 } // ISG
    // cache: 'no-store' // SSR
    // cache: 'force-cache' // SSG
  })

  await new Promise((resolve) => setTimeout(resolve, 2000))
  return res.json()
}
