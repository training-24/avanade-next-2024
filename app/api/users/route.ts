// DOC: https://nextjs.org/docs/app/building-your-application/routing/route-handlers
import { NextResponse } from 'next/server';


export async function GET(request: Request) {

  const url = new URL(request.url);
  const params = new URLSearchParams(url.searchParams)
  const name = params.get('name');

  const req= await fetch('https://jsonplaceholder.typicode.com/users')
  const res = await req.json();

  return NextResponse.json(res)
}

/*

export async function GET(request: Request) {
  return NextResponse.redirect(
    new URL(
      '/shop',
      request.url
    )
  )
}

*/
