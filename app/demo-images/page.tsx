// import dynamic from 'next/dynamic';
import dynamic from 'next/dynamic';
import emote from './next.png';

import Image from 'next/image';
export default function DemoImages () {
 //  const DynanicIcon = dynamic( () => import(path).then((obj) => obj.default) );

  return <div>
    <Image
      src={'/images/emote.png'}
      quality={20}
      width={200}
      height={200}
      alt="Emote Fabio"
    />

    <Image
      src={emote}
      placeholder="blur"
      quality={20}
      width={200}
      alt="Emote Fabio"
    />

    <Image
      src={require('./next.png')} alt=""
      placeholder="blur"
    />

  </div>
}

