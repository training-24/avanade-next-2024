'use server'

export type Todo =  {
  id: number;
  title: string;
  completed: boolean;
}

export async function getTodos(): Promise<Todo[]> {
  const res = await fetch('https://json-server-vercel-for-tutorials.vercel.app/todos')
  return await res.json()
}
