'use client';
import { getTodos, Todo } from '@/app/demo-server-actions/actions';
import { useState } from 'react';

export default function SimpleTodos () {
  const [todos, setTodos] = useState<Todo[]>([]);

  async function getTodo() {
    const res = await getTodos()
    setTodos(res)
  }

  return <div>
    <button
      className="btn"
      onClick={getTodo}
    >LOAD TODO</button>

    {
      todos.map(t => <li key={t.id}>{t.title}</li>)
    }

  </div>
}

