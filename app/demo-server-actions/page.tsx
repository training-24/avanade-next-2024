import { getTodos } from '@/app/demo-server-actions/actions';
import SimpleTodos from '@/app/demo-server-actions/components/SimpleTodos';
import Title from '@/components/shared/Title';

export default function Page () {

  return <div>

    <Title>Server actions</Title>

    <SimpleTodos />

    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias amet dolorum, eaque explicabo impedit molestias mollitia, obcaecati porro provident quae sapiente voluptate. Architecto dolore dolorem eum magnam molestiae ullam voluptate.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias amet dolorum, eaque explicabo impedit molestias mollitia, obcaecati porro provident quae sapiente voluptate. Architecto dolore dolorem eum magnam molestiae ullam voluptate.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias amet dolorum, eaque explicabo impedit molestias mollitia, obcaecati porro provident quae sapiente voluptate. Architecto dolore dolorem eum magnam molestiae ullam voluptate.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias amet dolorum, eaque explicabo impedit molestias mollitia, obcaecati porro provident quae sapiente voluptate. Architecto dolore dolorem eum magnam molestiae ullam voluptate.
  </div>
}

