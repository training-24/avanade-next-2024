// app/demo/page.tsx
import { User } from '@/model/user';

export async function getUsers(): Promise<User[]> {
  //const res = await fetch('https://jsonplaceholder.typicode.com/users');
  const res = await fetch('http://localhost:3000/api/users');
  return res.json();
}

export default async function Demo3() {
  const data = await getUsers();

  return (
    <main>
      {data?.map((user) => (
        <li key={user.id}>{user.name}</li>
      ))}
    </main>
  )
}
