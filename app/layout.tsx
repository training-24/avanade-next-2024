import Navbar from '@/components/navbar';
import { LanguageContextProvider } from '@/store/LanguageContextProvider';
import { ProfileContextProvider } from '@/store/ProfileContextProvider';
import type { Metadata } from "next";
import { Inter } from "next/font/google";
import "./globals.css";
import Script from 'next/script';

const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "Avanade Site",
  description: "bla bla... ",
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en">
      <LanguageContextProvider>
        <ProfileContextProvider>
          <body className={inter.className}>
            <Navbar/>

            <div className=" max-w-screen-xl overflow-hidden mx-6 xl:mx-auto mt-6 ">
             {children}
            </div>

            <footer className="  bottom-0 w-full text-center p-3">
              Fabio Biondi | @Copyright 2024
            </footer>

          </body>
        </ProfileContextProvider>
      </LanguageContextProvider>
    </html>
  );
}
