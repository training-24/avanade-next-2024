import Image from 'next/image';

export default function NotFound () {
  return <div className="flex flex-col items-center gap-10">
    PAGE NOT FOUND
    <Image
      src={'/images/emote.png'}
      quality={20}
      width={200}
      height={200}
      alt="Emote Fabio"
    />
  </div>
}

