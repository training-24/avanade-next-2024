import Hero from '@/components/widgets/hero';
import LatestPosts from '@/components/widgets/latest-posts';
import LatestVideoCourses from '@/components/widgets/latest-video-courses';
import LatestVideos from '@/components/widgets/latest-videos';
import Newsletter from '@/components/widgets/newsletter/newsletter';
import { Suspense } from 'react';

export default async function Home() {

  return (
    <div>
      <div className="bg-slate-800">
        <Hero />
      </div>

      <div className="flex flex-col sm:flex-row gap-3">
        <div className="w-1/3">
          <LatestVideoCourses />
        </div>

        <div className="w-1/3">
          <Suspense fallback={<Skeleton />}>
            <LatestVideos />
          </Suspense>
        </div>

        <div className="w-1/3">
          <Suspense fallback={<Skeleton />}>
            <LatestPosts />
          </Suspense>
        </div>
      </div>

      <div className="mt-8">
        <Newsletter/>
      </div>
    </div>
  );
}

function Skeleton() {
  return (
    <div role="status" className="max-w-sm animate-pulse">
      <div className="h-2.5 bg-gray-200 rounded-full dark:bg-gray-700 w-48 mb-4"></div>
      <div className="h-2 bg-gray-200 rounded-full dark:bg-gray-700 max-w-[360px] mb-2.5"></div>
      <div className="h-2 bg-gray-200 rounded-full dark:bg-gray-700 mb-2.5"></div>
      <div className="h-2 bg-gray-200 rounded-full dark:bg-gray-700 max-w-[330px] mb-2.5"></div>
      <div className="h-2 bg-gray-200 rounded-full dark:bg-gray-700 max-w-[300px] mb-2.5"></div>
      <div className="h-2 bg-gray-200 rounded-full dark:bg-gray-700 max-w-[360px]"></div>
      <span className="sr-only">Loading...</span>
    </div>
  )
}
