'use client'
import Title from '@/components/shared/Title';
import { LanguageContext } from '@/store/LanguageContextProvider';
import { ProfileContext } from '@/store/ProfileContextProvider';
import { useContext } from 'react';

export default function Page () {
  const { setLanguage, language  } = useContext(LanguageContext)
  const { state: profile, dispatch  } = useContext(ProfileContext)

  return <div>
    <Title>Settings</Title>

    <div>Current Profile Name: {profile.username}</div>
    <button className="btn"
            onClick={() => dispatch({ type: 'CHANGE_USERNAME', payload: 'Fabio'})}>Update Name</button>
    <div>Current Language: {language}</div>
    <button className="btn" onClick={() => setLanguage('it')}> it </button>
    <button className="btn" onClick={() => setLanguage('en')}> en </button>
  </div>
}

