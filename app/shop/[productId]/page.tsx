import { getProduct, getProducts } from '@/api/products.api';
import Title from '@/components/shared/Title';
import { TOPIC_IMAGES } from '@/model/product';
import Image from 'next/image';

type MyParams = {
  params: {
    productId: string
  }
}

// Static Dynamic Routes
// (remove to enable SSR)
export async function generateStaticParams() {
  const products = await getProducts();
  return products.map(p => ({ productId: p.id.toString() }))
}

export default async function ProductPage(props: MyParams) {
  const product = await getProduct(+props.params.productId);

  if (!product.id) {
    return <div> not found </div>
  }

  return <div>

    <Image src={TOPIC_IMAGES[product.topic]} alt={product.name} width={100} height={100} />
    <Title>{product.name}</Title>

    <pre>{JSON.stringify(product)}</pre>
  </div>
}

