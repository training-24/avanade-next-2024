'use client'
import { Product } from '@/model/product';
import { useCart } from '@/store/cart.store';
import React from 'react';
import css from './AddToCartButton.module.css';

interface AddToCartButtonProps {
  product: Product;
}

export default function  AddToCartButton (props: AddToCartButtonProps) {
  const addToCart = useCart(state => state.addToCart)

  return <>
    <button
      onClick={(e) => {
      e.preventDefault()
      e.stopPropagation()
      addToCart(props.product)
    }}
      color="cyan"
      className={css.btn}>add</button>
  </>
}

