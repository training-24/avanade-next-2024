'use client'

import { usePathname, useRouter, useSearchParams } from 'next/navigation';

let timer: any;

export default function Search () {
  const { replace } = useRouter()
  const pathname = usePathname()
  const searchParams = useSearchParams()

  function handleSearch(text: string) {
    clearTimeout(timer);
    timer = setTimeout(() => {
      const params = new URLSearchParams(searchParams);

      if (text) {
        params.set('query', text)
      } else {
        params.delete('query')
      }
      replace(`${pathname}?${params.toString()}`)
    }, 1000)

  }

  return (
    <div className="flex justify-center my-8">
      <input
        type="text"
        className="rounded-xl text-xl text-black p-3"
        onChange={(e) => handleSearch(e.target.value)}
        defaultValue={searchParams.get('query')?.toString()}
      />
    </div>
  )
}

