import { getProducts, searchProduct } from '@/api/products.api';
import AddToCartButton from '@/app/shop/components/AddToCartButton';
import Search from '@/app/shop/components/Search';
import { TOPIC_IMAGES } from '@/model/product';
import Image from 'next/image';
import Link from 'next/link';
import React from 'react';

type PageProps = {
  searchParams: {
    query?: string;
  }
}

export default async  function Page({ searchParams }: PageProps) {
  const products = await searchProduct(searchParams.query || '');

  return <div>
    <Search />

    <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 gap-3">
    {
      products.map(p => {
        return <div key={p.id}>
          <Link href={`/shop/${p.id}`}
                       className="flex flex-col items-center border border-slate-500 rounded-xl">
            <Image src={TOPIC_IMAGES[p.topic]} alt={p.name} width={100} height={100}/>
            {p.name}

            <AddToCartButton product={p} />
            {/*<IconAdd onClick={addToCart} color="cyan" className="border rounded-xl p-1" myTitle="ciao" />*/}

          </Link>
        </div>
      })
    }
    </div>
  </div>
}

interface IconAddProps {
  myTitle?: string;
  color?: string;
}

/**
 *
 * @param props
 * @constructor
 */
function IconAdd(props: IconAddProps & React.HTMLAttributes<HTMLOrSVGElement>) {
  const { myTitle, color, ...rest } = props
  return (
    <svg
      {...rest}
      viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" width={30}>
      <g id="SVGRepo_bgCarrier" strokeWidth={0}/>
      <g id="SVGRepo_tracerCarrier" strokeLinecap="round" strokeLinejoin="round"/>
      <g id="SVGRepo_iconCarrier">
        {' '}
        <path
          d="M21 5L19 12H7.37671M20 16H8L6 3H3M16 5.5H13.5M13.5 5.5H11M13.5 5.5V8M13.5 5.5V3M9 20C9 20.5523 8.55228 21 8 21C7.44772 21 7 20.5523 7 20C7 19.4477 7.44772 19 8 19C8.55228 19 9 19.4477 9 20ZM20 20C20 20.5523 19.5523 21 19 21C18.4477 21 18 20.5523 18 20C18 19.4477 18.4477 19 19 19C19.5523 19 20 19.4477 20 20Z"
          stroke={color || '#fff'}
          strokeWidth={2}
          strokeLinecap="round"
          strokeLinejoin="round"
        />
        {' '}
      </g>
    </svg>
  )
}
