'use client'
import { useCart } from '@/store/cart.store';
import { LanguageContext } from '@/store/LanguageContextProvider';
import { ProfileContext } from '@/store/ProfileContextProvider';
import clsx from 'clsx';
import Link from 'next/link';
import { usePathname } from 'next/navigation';
import { useContext } from 'react';

export default function Navbar () {
  /*const listCartItems = useCart(state => state.list)*/
  const totalCartItems = useCart(state => state.list.length)
  const {  language } = useContext(LanguageContext)
  const { state: profile  } = useContext(ProfileContext)

  const path = usePathname()

  return <nav className="flex flex-row gap-3 bg-slate-600 p-3 justify-between">
    <div className="flex gap-3">
      <Link
        href="/"
        className={clsx('btn', {
          'active': path === '/',
        })}
      >home</Link>
      <Link
        className={clsx('btn', {
          'active': path === '/about',
        })}
        href="/about"
      >about</Link>

      <Link
        href="/shop"
        className={clsx('btn', {
          'active': path === '/shop',
        })}
      >shop</Link>
      <Link
        href="/demo-server-actions"
        className={clsx('btn', {
          'active': path === '/demo-server-actions',
        })}
      >server actions</Link>
    </div>
    <div>
      Cart: {totalCartItems} - {language} - {profile.username}
    </div>
  </nav>
}

