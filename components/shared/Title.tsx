import { PropsWithChildren } from 'react';
import css from "./Title.module.css";

export default function Title (props: PropsWithChildren) {
  return <>
    <h1 className={css.title}>{props.children}</h1>
  </>
}

