import { getDevToPosts } from '@/api/devto-posts.api';
import Title from '@/components/shared/Title';

export default async function LatestPosts () {
  const posts = await getDevToPosts()
  // console.log(posts)

  return <div>
    <Title>Posts</Title>

    {
      posts.slice(0, 3).map(post => {
        return <div key={post.id}>
          {post.title}
        </div>
      })
    }
  </div>
}

