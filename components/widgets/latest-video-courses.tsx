import Title from '@/components/shared/Title';

const videos = [
  {
    id: 1,
    name: 'Angular Evolution'
  },
  {
    id: 2,
    name: 'React Pro'
  },
  {
    id: 3,
    name: 'Cypress'
  },
]

export default function LatestVideoCourses () {
  return <div>
  <Title>Video Courses</Title>

    {
      videos.map(v => {
        return <div key={v.id}>
          {v.name}
        </div>
      })
    }
  </div>
}

