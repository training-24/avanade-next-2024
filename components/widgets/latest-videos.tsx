import { getYoutubeVideos } from '@/api/youtube-videos.api';
import Title from '@/components/shared/Title';
import Image from 'next/image';

export default async  function LatestVideos () {
  const videos = await getYoutubeVideos()

  return <div>
    <Title>Latest Videos</Title>

    <div className="grid grid-cols-2 gap-3">
    {
      videos?.map(v => {
        const url = 'https://www.youtube.com/watch?v=' + v.id
        const img = `https://img.youtube.com/vi/${v.id}/0.jpg`

        return <a href={url} key={v.id} target="_blank">
          <Image
            src={img}
            alt={v.title}
            width={100}
            height={50}
          />
        </a>
      })
    }
    </div>
  </div>
}

