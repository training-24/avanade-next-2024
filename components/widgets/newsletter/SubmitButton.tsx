'use client'
import { useFormStatus } from 'react-dom';

export default function SubmitButton () {
  const obj  = useFormStatus()

  return <>
    <button
      className="btn"
      type="submit"
      disabled={obj.pending}
    >SEND</button>
  </>
}

