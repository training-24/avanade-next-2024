'use server';

import { z } from 'zod';

export async function send(
  prevState: any,
  formData: FormData
) {
  try {
    const schema = z.object({
      email: z.string().min(3)
    })

    const parse = schema.safeParse({
      email: formData.get('email')
    })

    if (!parse.success) {
      return { message: 'validation failed', status: 0}
    }

    const email =  formData.get('email');

    const response = await fetch('https://json-server-vercel-for-tutorials.vercel.app/send', {
      method: 'GET',
    })
    // 200-299
    if(!response.ok) {
      return { message: `Failed #2`, status: 403 }
    }

    return { message: `OK! Email subscribed: ${email}`, status: 200 }
  } catch (error) {
    return { message: `Failed #1`, status: 404 }
  }
}
