'use client'
import { send } from '@/components/widgets/newsletter/newsletter.action';
import SubmitButton from '@/components/widgets/newsletter/SubmitButton';
import { useFormState } from 'react-dom';

const initialState = {
  message: '',
  status: 0
}

export default function Newsletter () {
  const [state, formAction] = useFormState(send, initialState)

  return <div>
    MSG: {state.message} - {state.status}
    <form action={formAction}>
      <input type="text" name="email" className="text-black" />
      <SubmitButton />
    </form>
  </div>
}

