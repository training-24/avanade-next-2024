import { NextRequest, NextResponse } from 'next/server';

export function middleware(request: NextRequest) {
  let response = NextResponse.next()
  response.headers.set('Authorization', 'Bearer 123')
  return response;

  // Update response (with cookies)
  /*
  let response = NextResponse.next()
  response.cookies.set('show-xyz', 'false')
  return response;
  */

  // rewrite
  // return NextResponse.rewrite(new URL('/about', request.url))

  // redirect
  // return NextResponse.redirect(new URL('/about', request.url))

  // do nothing
  // return NextResponse.next()
}

export const config = {
  matcher: [
    '/demo/:path*',
    '/api/users/:path*',
  ]
}
