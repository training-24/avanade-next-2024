export type Topic = 'angular' | 'react' | 'misc';

export type Product = {
  id: number;
  name: string;
  topic: Topic;
}

export const TOPIC_IMAGES = {
  'angular': '/images/icons/angular.png',
  'react': '/images/icons/react.png',
  'misc': '/images/icons/emotejs.png',
}
