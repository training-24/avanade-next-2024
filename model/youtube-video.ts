export interface YouTubeVideo {
  id: string;
  title: string;
  duration: string;
}
