'use client';

import { createContext, PropsWithChildren, useContext, useState } from 'react';

export type LanguageState = 'it' | 'en';

// create context
export const LanguageContext = createContext({
  language: 'it',
  setLanguage: (state: LanguageState) => {}
})


export const LanguageContextProvider = (props: PropsWithChildren) => {
  const [language, setLanguage] = useState<LanguageState>('it')

  return <LanguageContext.Provider value={{ language, setLanguage }}>
    {props.children}
  </LanguageContext.Provider>
}

